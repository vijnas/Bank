package com.wipro;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.lang.*;

@Controller
public class BankController {

	@RequestMapping(value="/login.html" ,method=RequestMethod.GET)
	public ModelAndView LoginC (@RequestParam(value="username") String usern, @RequestParam(value="password") String passwrd)
	{
		System.out.println("Login");
	if(usern.equalsIgnoreCase("admin")&& passwrd.equalsIgnoreCase("admin"))
	{
		String message="Welcome "+usern;
		return new ModelAndView("BankAccount","message",message);
	}
	else
	{
		String message="Invalid user";
		return new ModelAndView("Hellopage","message",message);
	}
	}
		
	@RequestMapping(value="/calculate.html")
	public ModelAndView showCalc(@RequestParam("dropDown") String operator){
		
		if (operator.equalsIgnoreCase("Withdrawal")) {
			return new ModelAndView("Withdraw");				
		}
		else if (operator.equalsIgnoreCase("Deposit")) {
			return new ModelAndView("Deposit");
		}
		else {
			return new ModelAndView("BalanceEnq","message","10000");	
		}
		
	}
	
	@RequestMapping(value="/withdraw.html")
	public ModelAndView Withdrawal(@RequestParam("cash") String cashw){
		int w = Integer.parseInt(cashw);
		int amount=0;
		if(w < 10000)
		{
		amount=10000-w;
		String finalres=String.valueOf(amount);
		System.out.println("Final Result:   "+finalres);
		return new ModelAndView("Result","message",finalres);
		}
		else
		{
			return new ModelAndView("Hellopage","message","Insufficient Balance!!!!");
		}
		
	}
	

	@RequestMapping(value="/deposit.html")
	public ModelAndView Deposit(@RequestParam("cash") String cashd){
		
		int amount=0;
		int d = Integer.parseInt(cashd);
		if(d > 0)
		{
		amount=10000+d;
		String finalres=String.valueOf(amount);
		System.out.println("Final Result:   "+finalres);
		return new ModelAndView("Result","message",finalres);
		}
		else
		{
			return new ModelAndView("Hellopage","message","Enter Amount!!!!");
		}
		
	}
	
	
	
}
